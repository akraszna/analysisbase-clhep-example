// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

// CLHEP include(s).
#include <CLHEP/Random/Random.h>

// System include(s).
#include <iostream>
#include <cstdlib>

int main() {

   // Create a random number generator, and print some random numbers.
   CLHEP::HepRandom rnd{1234};
   std::cout << "Random numbers: " << rnd.flat() << ", " << rnd.flat()
             << std::endl;

   // Return gracefully.
   return EXIT_SUCCESS;
}
