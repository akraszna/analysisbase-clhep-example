# AnalysisBase CLHEP Example

This is an example for using CLHEP on top of an AnalysisBase release, as part
of someone's analysis.

To build, simply do something like:

```
asetup AnalysisBase,24.2.32
cmake -S analysisbase-clhep-example/ -B build/
cmake --build build/
```
