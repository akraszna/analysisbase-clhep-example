# "Package" building CLHEP as part of this project

This is a relatively simple way of building CLHEP as part of this project,
and making the CLHEP library target available for other parts of the project
to use with `CLHEP::CLHEP`.
