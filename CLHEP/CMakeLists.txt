# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Package building CLHEP as part of this project.
#

# Silence ExternalProject warnings with CMake >=3.24.
if(POLICY CMP0135)
   cmake_policy(SET CMP0135 NEW)
endif()

# CMake include(s).
include(FetchContent)

# Declare where to get CLHEP from.
FetchContent_Declare(
   CLHEP
   URL "https://gitlab.cern.ch/atlas-sw-git/CLHEP/-/archive/CLHEP_2_4_1_3_atl04/CLHEP-CLHEP_2_4_1_3_atl04.tar.gz"
   URL_MD5 "d2f9ea9f3368d2dd0321457a222c3f7e"
   OVERRIDE_FIND_PACKAGE)

# Tweak the CLHEP build options.
set(CLHEP_BUILD_DOCS OFF CACHE BOOL "(Don't) Build CLHEP documentation.")

# Set up the build.
FetchContent_MakeAvailable(CLHEP)

# Make CLHEP::CLHEP actually usable.
add_library(CLHEP::CLHEP ALIAS CLHEP)
target_include_directories(CLHEP
   INTERFACE $<BUILD_INTERFACE:${clhep_BINARY_DIR}>)
